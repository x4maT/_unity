﻿// ***********************************************************************
// Assembly         : _unity
// Author           : Abhishek Deb
// Created          : 05-16-2015
//
// Last Modified By : Abhishek Deb
// Last Modified On : 05-16-2015
// ***********************************************************************
// <copyright file="UI.cs" company="Skipbits">
//     Copyright ©  2015
// </copyright>
// <summary>
//      Use to enhance UI Options and tweaks such as fading and etc.
//      FOr Unity 4.6 + only.
// </summary>
// ***********************************************************************

using UnityEngine;
using System.Collections;
using _unity;

/// <summary>
/// The _unity namespace.
/// </summary>
namespace _unity.UI
{
    public class _UI : Singleton<_UI>
    {
        protected _UI() { }

        public void init()
        {
            _.l("_UI initialized");
        }



        /// <summary>
        /// Fades In the UI.
        /// </summary>
        /// <param name="G">The GameObject with Canvas/Canvas Group.</param>
        /// <param name="time">The time.</param>
        public void FadeInUI(GameObject G, float time)
        {
            CanvasGroup cg = G.GetComponent<CanvasGroup>();
            if (cg == null)
                cg = G.AddComponent<CanvasGroup>();

            StartCoroutine(FadeUI(cg, time));
        }

        /// <summary>
        /// Fades  out  the UI.
        /// </summary>
        /// <param name="G">The GameObject.</param>
        /// <param name="time">The time.</param>
        public void FadeOutUI(GameObject G, float time)
        {
            CanvasGroup cg = G.GetComponent<CanvasGroup>();
            if (cg == null)
                cg = G.AddComponent<CanvasGroup>();

            StartCoroutine(FadeUI(cg, time, false));
        }





        /// <summary>
        /// Consistent GUI Scaling across all scenes
        /// </summary>
        /// <param name="customWidth">The width of the app</param>
        /// <param name="customHeight">The height of the app</param>
        /// <param name="Callback">The callback.</param>
        /// <example>
        /// void OnGUI(){ _.GUISetup(800f,480f); }
        /// </example>
        public void OldGUIScale(float customWidth = 1366f, float customHeight = 768f, System.Action Callback = null)
        {
            GUI.matrix = Matrix4x4.TRS(Vector3.zero, Quaternion.identity, new Vector3(Screen.width / customWidth, Screen.height / customHeight, 1f));
            if (Callback != null) Callback();
        }

        #region Private Methods
        /// <summary>
        /// Fades the UI.
        /// </summary>
        /// <param name="G">The g.</param>
        /// <param name="time">The time.</param>
        /// <param name="FadeIn">if set to <c>true</c> [fade in].</param>
        /// <returns></returns>
        IEnumerator FadeUI(CanvasGroup G, float time, bool FadeIn = true)
        {
            if (FadeIn)
            {
                G.alpha = 0;
                while (G.alpha < 1f)
                {
                    G.alpha += time * Time.deltaTime;
                    yield return null;
                }
            }
            else
            {
                while (G.alpha > 0)
                {
                    G.alpha -= time * Time.deltaTime;
                    yield return null;
                }
            }

        }
        #endregion

    }
}
