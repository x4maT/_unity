_Unity
================

Utility Library for Unity Game Engine

*Update 16th September 2015 : *
- Added Unity 5 Support
- Added new Unity UI Extensions
- Cleaned some Floating point errors


*Update 30th April 2015 : *
- Added Finite State Machine Manager
- Examples in DOcumentation Added
- Lots of bugs fixed
- Many more code optimization


*Update 27th April 2015 : *
- Added Rest Wrapper
- Segrated all classes into important and relevant and different Classes and namespaces
- CHM Documentation Generated
- More COmments added for easy navigation and usage
- Lots of bugs fixed
- Removed unused Parametrs from Rest
- Many more code optimization

*UPDATE 10th Feb 2015 : * 
- Little More BUg FIxes
- New Method added for Singleton Management
- New Method Added for System Messaging
- Callback added for GUI Scaling
- Removed unused Camera Aspect Methods


*UPDATE 20th Dec 2014 : * 
- New Method for Initializing Array Objects
- Optimization Fixes
- Removed unused ExecOnce Method
- Random Generator Methods Added


*UPDATE 23rd Sept 2014 : *

- _.l  for Quick and easy debug
- GUISetup for easy Unity GUI Scaling

Put the _.cs file into Assets/Plugins directory and use it directly.





** Method List **

1. _.l(string message)
2. GUISetup(float width, float height [, Action Callback])
3. T[] InitializeArrayObject<T>(int length)
4. Shuffle<T>(this IList<T> list)
5. GetOrAddComponent(this Component child)


**Class List**

1. _
2. Singleton
3. Message
4. Rest
5. FSM


**EXAMPLE**

```
using _unity;

. . . // Class code 


//Prints Hello with detailed Date and time
_.l("Hello");


//Sets the GUI Scaling to 800x x 400 Resolution
_.GUISetup(800f,480f);


. . .


using  _unity.Net

. . .  // Class Code

Rest r = new Rest();

r.GET("http://localhost/name",delegate(Object data){ 

	data=JSON.Parse(data);
	
	print(data["name"]);

});


 . .  . // rest code


```